#!/bin/sh

cdrdao read-toc -v 3 data.toc
cueconvert data.toc data.cue
cdparanoia -v -O $1 1- data.wav
flac -V --cuesheet=data.cue -S- -8 --no-padding data.wav
metaflac --export-cuesheet-to=- data.flac | diff -w -B data.cue -
